import axios from 'axios';
import * as THREE from 'three';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
const renderer = new THREE.WebGLRenderer();
renderer.setSize(1000,600);
document.getElementById('paintPanel').appendChild(renderer.domElement);
document.getElementById('btn2').addEventListener('click', () => {
	let segments = parseInt(document.getElementById('segments').value);
	let height = parseInt(document.getElementById('height').value);
	let radius = parseInt(document.getElementById('radius').value);
	const data = [segments, height, radius];
	axios.post('http://localhost:3000',
		{data},
		{headers: {'Access-Control-Allow-Origin': 'http://localhost:5173/'}}
	).then(function (response) {
		while (scene.children.length > 0) {
			scene.remove(scene.children[0]);
		}
		createCone(response.data);
		animate();
	});
});
const createCone = (array) => {
	let vertices = new Float32Array(array);
	const geometry = new THREE.BufferGeometry();
	geometry.setAttribute('position', new THREE.BufferAttribute(vertices, 3));
	geometry.computeVertexNormals();
	geometry.computeTangents();
	const material = new THREE.MeshPhongMaterial({ color: 0xff0000, side: THREE.DoubleSide });
	const mesh = new THREE.Mesh(geometry, material);
	const light1 = new THREE.PointLight(0xFFFFFF, 1000, 100);
	const light2 = new THREE.PointLight(0xFFFFFF, 1000, 100);
	light1.position.set(15, 0, 15);
	light2.position.set(-15, 0, -15);
	scene.add(mesh);
	scene.add(light1, light2);
}
camera.position.z = 10;
const controls = new OrbitControls(camera, renderer.domElement);
controls.update();
function animate() {
	requestAnimationFrame(animate);
	controls.update();
	renderer.render(scene, camera);
}