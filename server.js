const PORT = 3000;
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors');
app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true,
    }),
);
app.use(cors());
app.options('*', cors());
app.listen(PORT, () => {
    console.log('live on port ' + PORT);
});
const calculateTriangle = (radius, i, segments) => {
    return [
        radius * Math.cos(2 * Math.PI * i / segments),
        radius * Math.sin(2 * Math.PI * i / segments),
        0
    ];
}
const getVerticesArray = (segments, height, radius) => {
    const verticesArray = [];
    const topVertex = [0, 0, height];
    for (let i = 0; i < segments; i++) {
        verticesArray.push(...topVertex);
        verticesArray.push(...calculateTriangle(radius, i, segments));
        verticesArray.push(...calculateTriangle(radius, i + 1, segments));
    }
    for (let i = 0; i < segments; i++) {
        verticesArray.push(...calculateTriangle(radius, i, segments));
        verticesArray.push(...calculateTriangle(radius, i + 1, segments));
        verticesArray.push(0, 0, 0);
    }
    return verticesArray;
}
app.post('/', (request, responce) => {
    const data = request.body;
    const verticesArray = getVerticesArray(...data.data);
    responce.json(verticesArray);
});